const express = require("express");
const cors = require('cors');
var bodyParser = require('body-parser');
const dotenv = require("dotenv");
const mongoose = require("mongoose");

const app = express();

dotenv.config();

//DB connection.
mongoose.connect(
    process.env.DB_CONNECT,
    { useUnifiedTopology: true, useNewUrlParser: true }, () => console.log("Connected to DB")
);

//MiddleWares
app.use(express.json());

//Importing routes
const objectRoutes = require("./routes/object");

//Route middleware for Route.
app.use("/api/objects", objectRoutes);

app.listen(4000, () => {
    console.log("Server is listening....");
});