const mongoose = require("mongoose");

const objectSchema = new mongoose.Schema({    
    object_name : String,
    company_id : String,
    date_created : String,
    date_updated : String,
    last_modified_by : String,
    description : String ,
    object_type : String,
    columns : [
          {
            name: String,
            name_friendly: String,
            data_type : String,
            description : String,
            periodic: Boolean,
            formula : String
          }
        ],
        relationships : [
          {
            name: String,
            description : String,
            object : {
              name : String,
              id: String
            }
          }
        ]
      
});

//On server we have already added object db in collections.
//If we have not created it, it will add "s" at last and will create a new collection.
module.exports = mongoose.model("Object", objectSchema);