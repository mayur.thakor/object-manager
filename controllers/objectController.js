const ObjectModel = require("../model/Object");

// Get All objects
const object_all = async (req, res) => {
    try {
        // find is MongoDB method.
        const objects = await ObjectModel.find();
        // returnig response in the JSON format.
        res.json(objects)
    } catch (error) {
        res.json({ message: error });
    }
};

// Single object
const object_details = async (req, res) => {
    try {
        const objects = await ObjectModel.findById(req.params.objectId);
        res.json(objects)
    } catch (error) {
        res.json({ message: error });
    }
};

// Add New object
const object_create = async (req, res, next) => {

    //Data passed from FE will be capture in the req.body
    const new_object = new ObjectModel({
        object_name : req.body.object_name,
        company_id : req.body.company_id,
        date_created : req.body.date_created,
        date_updated : req.body.date_updated,
        last_modified_by : req.body.last_modified_by,
        description : req.body.description ,
        object_type : req.body.object_type,
        columns : req.body.columns,
        relationships : req.body.relationships
    });

    //save is Mongodb method.
    try{
        const saveObject = await new_object.save();
        res.send(saveObject);
    } catch (error) {
        res.status(400).send(error);
    }

};

// Update object
const object_update = async (req, res) => {
    
    try{
        //Data passed from FE will be capture in the req.body
        const edit_object = {
            object_name : req.body.object_name,
            company_id : req.body.company_id,
            date_created : req.body.date_created,
            date_updated : req.body.date_updated,
            last_modified_by : req.body.last_modified_by,
            description : req.body.description ,
            object_type : req.body.object_type,
            columns : req.body.columns,
            relationships : req.body.relationships
        };

        //findByIdAndUpdate is Mongodb method.
        const updatedObject = await ObjectModel.findByIdAndUpdate(
            { _id: req.params.objectId },
            edit_object
        );
        res.send(updatedObject);
    } catch (error) {
        res.json({ message: error });
    }
};

// Delete object
const object_delete = async (req, res) => {
    //findByIdAndDelete is Mongodb method.
    try{
        const deleteObject = await ObjectModel.findByIdAndDelete(req.params.objectId);
        res.send(deleteObject);
    } catch (error) {
        res.json({ message: error });
    }
};

module.exports = {
    object_all,
    object_details,
    object_create,
    object_update,
    object_delete
}