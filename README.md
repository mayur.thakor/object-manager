# Object-manager

Sample MS for Object Manager

## Intial set up and to start the server 

```
npm install
npm start
```

This should start the local server and the MS will attempt to connect to the database.

## Supported Routes
- Add new object:-  router.post("/");
- Get all Object:-  router.get("/", objectController.object_all);
- Get details of single object:- router.get("/:objectId", objectController.object_details);
- Update Object:- router.put("/:objectId", objectController.object_update);
- Delete Object:- router.delete("/:objectId",objectController.object_delete);

## Prerequisites
Get **REST Client** plugin for your Visual Code. It will be used to test run the API's through **test.http** file. A better alternate of postman.

> Note: Once the local server is up and running, go to test.http and click on 'Send request' text just above the method you want to call. Plugin should display the status of call.