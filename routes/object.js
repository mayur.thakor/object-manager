const router = require("express").Router();
const objectController = require('../controllers/objectController');

router.post("/",objectController.object_create);
router.get("/", objectController.object_all);
router.get("/:objectId", objectController.object_details);
router.put("/:objectId", objectController.object_update);
router.delete("/:objectId",objectController.object_delete);


module.exports = router;